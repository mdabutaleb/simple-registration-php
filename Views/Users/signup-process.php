<?php
include_once "../../vendor/autoload.php";
use App\Users\Users;
use App\Utility\Utility;

session_start();
//Utility::dd($_POST);
$obj = new Users();
if (strtoupper($_SERVER['REQUEST_METHOD']) == "POST") {
    if (!empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['email'])) {
        $obj->prepare($_POST);
    } else {
        $_SESSION['Message'] = "Something is missing...";
        header('location:../../signup.php');
    }
}